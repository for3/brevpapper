\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{for3}[2023/12/11 För3 brevpappersmall]

\newcommand\thepagestyle{for3}
\DeclareOption{sign}{\renewcommand\thepagestyle{for3sign}}
\ProcessOptions\relax

\LoadClass{article}

\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}

% Add showframe as an option for debugging purposes
\RequirePackage[a4paper,total={6in,8in}]{geometry}

\RequirePackage{fancyhdr}% Base for almost all style
\RequirePackage{graphicx}% Include logo
\RequirePackage{titling}% Enables the \thetitle and \thedate commands.

\RequirePackage{parskip}% Expected style
\RequirePackage{lastpage}% Get page count for page numbering
\RequirePackage{titlesec}% Allow styling of section headings
\RequirePackage{keyval}% For key-value arguments to \bilaga

\RequirePackage{datatool-base}% For DTLinitials, for extracting initials from name

\RequirePackage[final]{pdfpages}% For includepdf
\RequirePackage{hyperref}% For phantomsection

\titleformat{\section}{\ttfamily\Large}{\thesection}{1em}{}
\titleformat{\subsection}{\ttfamily\large}{\thesubsection}{1em}{}
\titleformat{\subsubsection}{\ttfamily\large}{\thesubsubsection}{1em}{}

\newcommand\orgnum{802529-7493}
\newcommand\logo{for3-logo}
\newcommand\contactinfo{%
	Föreningar för föreningsaktivas\\%
	Kårallen, Linköpings universitet\\%
	581 83 Linköping\\%
}

\renewcommand\maketitle{\PackageError{for3}%
{Maketitle is disabled in För3 Environment}%
{The title is already present on all pages (unless the page style is
changed). Adding an extra title is surupurflous, and (may) break page
styles.}}

% Both titling and titlesec provides the macro \thetitle
% titling provides it as the document title (as set per \title), which
% is what we want. titlesec however redefines it as the current
% headings title. This saves the documents title for use in all
% heading.
\AtBeginDocument{\let\thedocumenttitle\thetitle}

\fancypagestyle{for3base}{%
	\setlength\headheight{.15\textwidth}

	\lhead{\parbox{.15\textwidth}{\includegraphics[height=\linewidth]{\logo}}%
		\parbox[][.15\textwidth][c]{.5\textwidth}{\Large\ttfamily\thedocumenttitle}}
}

\fancypagestyle{for3}[for3base]{%
	\cfoot{}
	\lfoot{\small\ttfamily{Org. \#\orgnum}}
	\rfoot{\small\ttfamily{{\thepage}~{(\pageref*{LastPage})}}}

	\setlength\headheight{.15\textwidth}

	\rhead{\parbox[][.15\textwidth][t]{.5\textwidth}{%
		\small\ttfamily\raggedleft\contactinfo{}~\\{\thedate}\\
	}}
}

\fancypagestyle{for3sign}[for3base]{%
	\cfoot{\signfoot}

	\rhead{\parbox[][.15\textwidth][t]{.5\textwidth}{%
		\small\ttfamily\raggedleft\contactinfo{}%
		Org. \#\orgnum\\
		\vspace{1em}%
		{\thedate},
		{\thepage}~{(\pageref*{LastPage})}\\%
	}}
}

\newcommand\bilaganame{}
\newcommand\bilagapages{-}
\def\bilagainc{\inctall}

\makeatletter
\define@key{bilaga}{pages}{\renewcommand\bilagapages{#1}}
\define@key{bilaga}{wide}[true]{\def\bilagainc{\incwide}}
\makeatother

\newcommand\inctall[2]{\includepdf[#1,height=\textheight]{#2}}
\newcommand\incwide[2]{\includepdf[#1,width=\paperwidth]{#2}}

% Basically a re-implementation of hyperref's \label command.
% Needed since we want to reference sections which doesn't exsists,
% but hyperref always focuses the last "real" section.
% https://tex.stackexchange.com/questions/512148/reference-for-newlabel
\makeatletter
\def\coollabel#1{%
	\@bsphack
	\begingroup
		\def\label@name{#1}%
		\label@hook
		\protected@write\@auxout{}{%
			\string\newlabel{#1}{%
				{\thesection}%
				{\thepage}%
				{\@currentlabelname}%
				{\@currentHref}{}%
			}%
		}%
	\endgroup
	\@esphack
}%
\makeatother

% #1 extra args
% #2 title
% #3 label
% #4 file
\newcommand\bilaga[4][]{%
	\setkeys{bilaga}{#1}%
	\stepcounter{section}%
	\phantomsection%
	\coollabel{#3}%
	\renewcommand\bilaganame{#2}%
	\bilagainc{pages={\bilagapages},%
	frame,%
	offset=0cm -1cm,%
	pagecommand={%
		\thispagestyle{for3bilaga}%
	}}{#4}%
	\def\bilagainc{\inctall}%
}

% This automatically inherits from the default page style
\fancypagestyle{for3bilaga}{%
	\lhead{\parbox{.15\textwidth}{\includegraphics[height=\linewidth]{\logo}}%
		\parbox[][.15\textwidth][c]{.5\textwidth}{\ttfamily{\Large\thedocumenttitle}\\Bilaga~\thesection~\bilaganame}}
}

\pagestyle{\thepagestyle}

\newcommand\underskrivet{\vspace{1cm}\par\large\textsc\theauthor}

\makeatletter
\newcommand*\signatures{}
\newcommand*\for@sign[1]{%
	\ifx\signatures\empty
		\def\signatures{#1}%
	\else
		\g@addto@macro\signatures{#1}%
	\fi}

\newcommand*\signfoot{\hfill}
\newcommand*\for@signfoot[1]{%
	\g@addto@macro\signfoot{%
		\parbox{2cm}{%
			\vspace{2cm}\rule{2cm}{0.4pt}\center\DTLinitials{#1}
		}\hfill}}

\newcommand\signentry[2]{%
	\textbf{#1}\\[1.5cm]%
	\rule{.7\textwidth}{0.4pt}\\%
	#2\\[5mm]
}

\newcommand\ordforande[1]{%
	\for@sign{\signentry{Mötesordförande}{#1}}%
	\for@signfoot{#1}%
}

\newcommand\sekreterare[1]{%
	\for@sign{\signentry{Mötessekreterare}{#1}}%
	\for@signfoot{#1}%
}

\newcommand\justerare[1]{%
	\for@sign{\signentry{Justerare}{#1}}%
	\for@signfoot{#1}%
}

\makeatother
