För3 TeX-klass
=============

Klassparametrar
---------------

### `sign`

Dokumentet är ett mötesprotokoll eller liknande, och skall vara anpassat för
signering. Kom ihåg att använda makrot `\signatures` på lämpligt ställe.


Makron
------

### För allmän användning

#### `\bilaga[<inställningar>]{<titel>}{<etikett>}{<filnamn>}`
Infogar en annan PDF som bilaga.

- *titel* är det mänskligt läsbara namnet på bilagan
- *etikett* motsvarar TeX:s `\label`, och tillåter `\ref`.
- *filnamn* är vilken fil som skall inkluderas.

##### *Inställningar*

Följande övriga inställningar finns tillgängliga:

###### pages=*range*
Sidor av PDF:en vilka skall inkluderas. Skickas vidare till `\includepdf`.

###### wide
Parameter vilken antyder att det indragna dokumentet är brett istället för högt.
Påverkar marginaler.

#### `\signatures`
Infogar signeringslinjer för alla mötesfunktionärer. 

Mötesfunktionärer registreras med nedan nämnda kommandon, och visas i den
ordningen de anges. Dessa kommandon bör användas *innan* `\begin{document}`, men
kan alla användas flera gånger. Notera att Unicode inte fungerar optimalt med
signaturer. T.ex. behöver namnet "Örjan Börjesson" skrivas som 
`{Ö}rjan Börjesson`.

Om klassparametern `sign` är sätt kommer personernas initialer även läggas till
på varje sida.

##### `\ordforande{<namn>}`
##### `\sekreterare{<namn>}`
##### `\justerare{<namn>}`

#### `\underskrivet`
Infogar en underskrift utifall att dokumentet är ett brev. Använder det namnet
vilket sattes genom kommandot `\author`.

### Vilka kan omdefinieras
Följande makron finns med rimliga värden, men kan ändras om mallen skall passa
en annan förening.

#### `\orgnum`

Föreningens organisationsnummer, utan dekorationer.

**e.x.:** `801020-3040`.

#### `\logo`
Filnamn för föreningens logotyp, utan filändelse.

**e.x.:** `for3-logo`.

#### `\concatinfo`

Postaddress för föreningen.

**e.x.**:
```tex
Föreningsnamn\\%
Gatuaddress\\%
123 45 Stadsnamn\\%
```

Övrigt
------

`\maketitle` är bortplockat från tillgängliga makron, då rubrik och liknande
redan återfinns i sidhuvudet (samt för att göra sidhuvudhantering enklare).
