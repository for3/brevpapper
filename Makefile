TEXMF = $(HOME)/texmf
target_dir = $(TEXMF)/tex/latex/for3

install:
	install -m644 -D -t $(target_dir) for3-logo.pdf
	install -m644 -D -t $(target_dir) for3.cls

